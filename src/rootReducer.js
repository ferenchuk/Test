import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import groups from './stories/groups/reducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  form,
  groups,
});

export default rootReducer;