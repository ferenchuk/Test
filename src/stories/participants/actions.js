import axios from 'axios';
import {
  API_GET_PARTICIPANTS_LIST,
  API_DELETE_PARTICIPANT,
  API_CREATE_PARTICIPANT,
  API_GET_PARTICIPANT,
} from './../../constants/routes';
import { API_EDIT_PARTICIPANT } from '../../constants/routes';

export const addUser = data => (
  axios.post(API_CREATE_PARTICIPANT, data)
    .then(response => (response.data))
);

export const deleteUser = id => (
  axios.delete(`${API_DELETE_PARTICIPANT}/${id}`)
    .then(response => (response.data))
);

export const getUser = id => (
  axios.get(`${API_GET_PARTICIPANT}/${id}`)
    .then(response => (response.data))
);

export const fetchUsersRequest = query => (
  axios.get(API_GET_PARTICIPANTS_LIST, {
    params: query,
  })
);

export const editUser = data => (
  axios.put(`${API_EDIT_PARTICIPANT}/${data.id}`, data)
);
