import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ListItem extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
    this.showEdit = this.showEdit.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.state = {
      editable: false,
    };
  }

  showEdit() {
    this.setState({ editable: true });
  }

  onEdit() {
    const { editUser, id } = this.props;
    editUser({
      id,
      name: this.nameInput.value,
      surname: this.surNameInput.value,
      email: this.emailInput.value,
      phone: this.phoneInput.value,
    });
    this.setState({ editable: false });
  }

  onDelete() {
    const { onDelete, id } = this.props;
    onDelete(id);
  }

  render() {
    const { id, name, surname, email, phone } = this.props;
    const { editable } = this.state;
    if (editable) {
      return (
        <tr>
          <td><input type="text" name="name" id="name" defaultValue={name} ref={nameInput => this.nameInput = nameInput} /></td>
          <td><input type="text" name="surname" id="surname" defaultValue={surname} ref={surNameInput => this.surNameInput = surNameInput} /></td>
          <td><input type="email" name="email" id="email" defaultValue={email} ref={emailInput => this.emailInput = emailInput} /></td>
          <td><input type="text" name="phone" id="phone" defaultValue={phone} ref={phoneInput => this.phoneInput = phoneInput} /></td>
          <td className="options">
            <i className="fas fa-save" onClick={this.onEdit} />
          </td>
        </tr>
      )
    }
    return (
      <tr>
        <td><Link to={`users/${id}`}>{name}</Link></td>
        <td>{surname}</td>
        <td>{email}</td>
        <td>{phone}</td>
        <td className="options">
          <i className="fas fa-edit" onClick={this.showEdit} />
          <i className="fas fa-trash-alt" onClick={this.onDelete} />
        </td>
      </tr>
    );
  }
}

const propTypes = {};
const defaultProps = {};

ListItem.propTypes = propTypes;
ListItem.defaultProps = defaultProps;
