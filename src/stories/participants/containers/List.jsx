import React, { Component } from 'react';
import Popup from '../../../components/popup/Popup';
import ListItem from './ListItem';
import CreateForm from './CreateForm';
import { fetchUsersRequest, addUser, deleteUser, editUser } from '../actions';

export default class List extends Component {
  constructor(props) {
    super(props);

    this.fetchUsers = this.fetchUsers.bind(this);
    this.showAddUserPopup = this.showAddUserPopup.bind(this);
    this.createUser = this.createUser.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.handleEditUser = this.handleEditUser.bind(this);
    this.sortBy = this.sortBy.bind(this);

    this.state = {
      users: [],
      sortOrder: 'asc',
    };
  }

  componentDidMount() {
    this.fetchUsers();
  }

  fetchUsers(query) {
    fetchUsersRequest(query)
      .then(response => (this.setState({ users: response.data })));
  }

  handleEditUser(data) {
    const { users } = this.state;
    editUser(data);
    const index = users.findIndex(item => (item.id === data.id));
    this.setState({ ...users[index] = data })
  };

  onDelete(id) {
    const { users } = this.state;
    deleteUser(id);
    const filteredUsers = users.filter(user => user.id !== id);
    this.setState({ users: filteredUsers })
  }

  showAddUserPopup() {
    this.simpleDialog.show()
  };

  createUser(data) {
    addUser(data)
      .then(() => this.fetchUsers());
    this.simpleDialog.close();
  }

  onSearchChange(e) {
    this.fetchUsers({ q: e.target.value });
  }

  sortBy(field) {
    const { sortOrder } = this.state;
    this.fetchUsers({ '_sort': field, '_order': sortOrder });
    this.setState({ sortOrder: sortOrder === 'asc' ? 'desc' : 'asc' });
  }

  render() {
    const { users } = this.state;
    return (
      <div className="card">
        <div className="tab-header">
          <div className="col-lg-12">
            <div className="row align-items-center justify-content-between">
              <h2>Participants list</h2>
              <div>
                <input type="text" className="search" placeholder="Search" onChange={this.onSearchChange} />
                <i className="fas fa-2x fa-plus-square align-middle" onClick={this.showAddUserPopup} />
              </div>
            </div>
          </div>
        </div>
        <table>
          <colgroup>
            <col />
            <col />
            <col />
            <col />
            <col width="100px" />
          </colgroup>
          <thead>
          <tr>
            <th onClick={() => this.sortBy('name')}>Name</th>
            <th onClick={() => this.sortBy('surname')}>Surname</th>
            <th onClick={() => this.sortBy('email')}>Email</th>
            <th onClick={() => this.sortBy('phone')}>Phone</th>
            <th />
          </tr>
          </thead>
          <tbody>
            {users.map(user => <ListItem key={user.id} {...user} editUser={this.handleEditUser} onDelete={this.onDelete} />)}
          </tbody>
        </table>
        <Popup
          ref={ref => this.simpleDialog = ref}
          title="Create User"
        >
          <CreateForm onCreateClick={this.createUser} />
        </Popup>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

List.propTypes = propTypes;
List.defaultProps = defaultProps;
