import React, { Component } from 'react';
import { getUser } from './../actions';

export default class Preview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    getUser(id)
      .then(response => (this.setState({ user: response })));
    console.log(this.state.user);
  }

  render() {
    const { user } = this.state;
    return (
      <div className="card">
        <span className="label">Name: </span>
        { user.name }
        <hr/>
        <span className="label">Surname: </span>
        { user.surname }
        <hr/>
        <span className="label">Email: </span>
        { user.email }
        <hr/>
        <span className="label">Phone: </span>
        { user.phone }
        <hr/>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

Preview.propTypes = propTypes;
Preview.defaultProps = defaultProps;
