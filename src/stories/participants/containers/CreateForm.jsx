import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CreateForm extends Component {
  constructor(props) {
    super(props);

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.state = {};
  }

  onFormSubmit(event) {
    event.preventDefault();
    const { onCreateClick } = this.props;
    onCreateClick({
      name: this.nameInput.value,
      surname: this.surNameInput.value,
      email: this.emailInput.value,
      phone: this.phoneInput.value,
    });
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input type="text" name="name" id="name" ref={nameInput => this.nameInput = nameInput} />
        </div>
        <div className="form-group">
          <label htmlFor="surname">Surname</label>
          <input type="text" name="surname" id="surname" ref={surNameInput => this.surNameInput = surNameInput} />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input type="email" name="email" id="email" ref={emailInput => this.emailInput = emailInput} />
        </div>
        <div className="form-group">
          <label htmlFor="email">Phone</label>
          <input type="text" name="phone" id="phone" ref={phoneInput => this.phoneInput = phoneInput} />
        </div>
        <button className="btn" type="submit">Create</button>
      </form>
    );
  }
}

const propTypes = {
  onCreateClick: PropTypes.func,
};
const defaultProps = {};

CreateForm.propTypes = propTypes;
CreateForm.defaultProps = defaultProps;
