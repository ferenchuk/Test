const defaultState = {
  list: [],
  preview: {},
};

export default (state = defaultState, action) => {
  const { type, payload } = action;
  switch (type) {
    case 'STORE_GROUP_LIST':
      return {
        ...state,
        list: payload,
      };

    case 'STORE_GROUP':
      return {
        ...state,
        preview: { ...payload },
      };

    case 'DELETE_GROUP':
      const filteredGroups = state.list.filter(item => (item.id !== payload.id));
      return {
        ...state,
        list: filteredGroups,
      };

    case 'ADD_PARTICIPANT':
      return {
        ...state,
        preview: {
          ...state.preview,
          participants: state.preview.participants,
        },
      };

    case 'DELETE_PARTICIPANT':
      const filteredParticipants = state.preview.participants.filter(item => (item.id !== parseInt(payload.id)));
      return {
        ...state,
        preview: {
          ...state.preview,
          participants: filteredParticipants,
        },
      };

    default:
      return state
  }
};
