export const storeGroups = payload => ({
  type: 'STORE_GROUP_LIST',
  payload,
});

export const storeGroup = payload => ({
  type: 'STORE_GROUP',
  payload,
});

export const deleteGroup = id => ({
  type: 'DELETE_GROUP',
  payload: {
    id,
  },
});

export const deleteParticipant = id => ({
  type: 'DELETE_PARTICIPANT',
  payload: {
    id,
  },
});

export const addParticipant = user => ({
  type: 'ADD_PARTICIPANT',
  payload: {
    user,
  },
});
