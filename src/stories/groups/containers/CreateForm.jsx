import React, { Component } from 'react';

export default class CreateForm extends Component {
  constructor(props) {
    super(props);

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.state = {};
  }

  onFormSubmit(event) {
    event.preventDefault();
    const { onCreateClick } = this.props;
    onCreateClick({ name: this.nameInput.value });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input type="text" name="name" id="name" ref={nameInput => this.nameInput = nameInput} />
          </div>
          <button className="btn" type="submit">Create</button>
        </form>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

CreateForm.propTypes = propTypes;
CreateForm.defaultProps = defaultProps;
