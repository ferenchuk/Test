import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { storeGroups, deleteGroup as deleteGroupAction } from './../actionCreators';
import ListItem from './ListItem';
import { API_FETCH_GROUPS, API_CREATE_GROUP, API_DELETE_GROUP } from '../../../constants/routes';
import CreateForm from './CreateForm';
import Popup from '../../../components/popup/Popup';

class List extends Component {
  constructor(props) {
    super(props);

    this.fetchGroups = this.fetchGroups.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.showAddGroupPopup = this.showAddGroupPopup.bind(this);
    this.createGroup = this.createGroup.bind(this);
    this.deleteGroup = this.deleteGroup.bind(this);
    this.state = {
      sortOrder: 'asc',
    };
  }

  componentDidMount() {
    this.fetchGroups()
  }

  fetchGroups(query) {
    const { storeGroups } = this.props;
    axios.get(API_FETCH_GROUPS, {
      params: query,
    })
      .then(response => (storeGroups(response.data)));
  }

  onSearchChange(event) {
    this.fetchGroups({ 'q': event.target.value });
  }

  sortBy(field) {
    const { sortOrder } = this.state;
    this.fetchGroups({ '_sort': field, '_order': sortOrder });
    this.setState({ sortOrder: sortOrder === 'asc' ? 'desc' : 'asc' });
  }

  onDelete(id) {
    this.deleteGroup(id);
  }

  showAddGroupPopup() {
    this.simpleDialog.show();
  }

  createGroup(data) {
    axios.post(API_CREATE_GROUP, { ...data, participants: [] })
      .then(() => this.fetchGroups());
    this.simpleDialog.close();
  }

  deleteGroup = id => {
    const { deleteGroupAction } = this.props;
    return (
      axios.delete(`${API_DELETE_GROUP}/${id}`)
        .then(response => (deleteGroupAction(id)))
    )
  };

  render() {
    const { groups } = this.props;
    return (
      <div className="card">
        <div className="tab-header">
          <div className="col-lg-12">
            <div className="row align-items-center justify-content-between">
              <h2>Groups list</h2>
              <div>
                <input type="text" className="search" placeholder="Search" onChange={this.onSearchChange} />
                <i className="fas fa-2x fa-plus-square align-middle" onClick={this.showAddGroupPopup} />
              </div>
            </div>
          </div>
        </div>
        <table>
          <colgroup>
            <col />
            <col width="90px" />
          </colgroup>
          <thead>
          <tr>
            <th onClick={() => this.sortBy('name')}>Name</th>
            <th />
          </tr>
          </thead>
          <tbody>
          {groups.map(group => (<ListItem key={group.id} {...group} onDelete={this.onDelete} />))}
          </tbody>
        </table>
        <Popup
          ref={show => this.simpleDialog = show}
          title="Create Group"
        >
          <CreateForm onCreateClick={this.createGroup} />
        </Popup>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {
  groups: [],
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

const mapStateToProps = (state) => {
  return {
    groups: state.groups.list,
  }
};

export default connect(mapStateToProps, { storeGroups, deleteGroupAction })(List);
