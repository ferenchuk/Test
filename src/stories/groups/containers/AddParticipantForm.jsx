import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AddParticipantForm extends Component {
  constructor(props) {
    super(props);

    this.addUser = this.addUser.bind(this);

    this.state = {};
  }

  addUser(event) {
    const { addUser, users } = this.props;
    const user = users.find(item => (item.id === parseInt(event.target.id)));
    addUser(user);
  }

  render() {
    const { users } = this.props;
    return (
      <div>
        {users.map(item => (
          <div className="list-item">
            {item.name} {item.surname}
            <i id={item.id} className="fas fa-plus-square" onClick={this.addUser} />
          </div>
        ))}
      </div>
    );
  }
}

const propTypes = {
  participants: PropTypes.array,
};
const defaultProps = {
  participants: [],
};

AddParticipantForm.propTypes = propTypes;
AddParticipantForm.defaultProps = defaultProps;
