import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  storeGroup,
  deleteParticipant as deleteParticipantAction,
  addParticipant,
} from './../actionCreators';
import {
  API_FETCH_GROUPS,
  API_GET_PARTICIPANTS_LIST,
  API_UPDATE_GROUP,
} from '../../../constants/routes';
import Participant from './Participant';
import Popup from '../../../components/popup/Popup';
import AddParticipantForm from './AddParticipantForm';

class Preview extends Component {
  constructor(props) {
    super(props);

    this.fetchGroup = this.fetchGroup.bind(this);
    this.showAddParticipantPopup = this.showAddParticipantPopup.bind(this);
    this.deleteParticipant = this.deleteParticipant.bind(this);
    this.addUser = this.addUser.bind(this);

    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    this.fetchGroup(id);
    axios.get(API_GET_PARTICIPANTS_LIST)
      .then(response => (this.setState({ users: response.data })))
  }

  showAddParticipantPopup() {
    this.simpleDialog.show();
  }

  deleteParticipant(id) {
    const { group, deleteParticipantAction, match } = this.props;
    const { participants } = group;
    const filteredParticipants = participants.filter(item => (item.id !== parseInt(id)));
    axios.put(`${API_UPDATE_GROUP}/${match.params.id}`, { ...group, participants: filteredParticipants })
      .then(response => (deleteParticipantAction(id)));
  }

  fetchGroup = id => {
    const { storeGroup } = this.props;
    return (
      axios.get(`${API_FETCH_GROUPS}/${id}`)
        .then(response => (storeGroup(response.data)))
    )
  };

  addUser(user) {
    const { match, group, addParticipant } = this.props;
    const participants = group.participants || [];
    participants.push(user);
    axios.put(`${API_UPDATE_GROUP}/${match.params.id}`, {
      ...group,
      participants,
    }).then(() => {
      addParticipant(user);
    })
  }

  render() {
    const { group } = this.props;
    const { name, participants } = group;
    const { users } = this.state;
    const filteredUsers = _.differenceWith(users, participants, _.isEqual);
    debugger;
    return (
      <div>
        <div className="card">
          <div className="row align-items-center justify-content-between">
            <h2 className="col-lg-10">{name}</h2>
            <i className="fas fa-2x fa-plus-square align-middle" onClick={this.showAddParticipantPopup} />
          </div>
        </div>
        { participants ? participants.map(item => (<Participant key={item.key} {...item} deleteParticipant={this.deleteParticipant} />)) : <div className="text-center">Empty</div>}
        <Popup
          ref={show => this.simpleDialog = show}
          title="Add User to group"
        >
          <AddParticipantForm users={filteredUsers} addUser={this.addUser} />
        </Popup>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {
  participants: [],
  users: [],
};

Preview.propTypes = propTypes;
Preview.defaultProps = defaultProps;

export default connect(state => ({
  group: state.groups.preview,
}), {
  storeGroup,
  deleteParticipantAction,
  addParticipant,
})(Preview);
