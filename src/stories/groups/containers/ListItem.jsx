import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ListItem extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
    this.state = {};
  }

  onDelete() {
    const { onDelete, id } = this.props;
    onDelete(id);
  }

  render() {
    const { id, name } = this.props;
    return (
      <tr>
        <td>
          <Link to={`groups/${id}`}>{name}</Link>
        </td>
        <td className="options">
          <i className="fas fa-trash-alt" onClick={this.onDelete} />
        </td>
      </tr>
    );
  }
}

const propTypes = {};
const defaultProps = {};

ListItem.propTypes = propTypes;
ListItem.defaultProps = defaultProps;
