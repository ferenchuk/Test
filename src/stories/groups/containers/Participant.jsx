import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Participant extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);

    this.state = {};
  }

  onDelete(event) {
    const { deleteParticipant } = this.props;
    const participantId = event.target.id;
    deleteParticipant(participantId);
  }

  render() {
    const { id, name, surname, email, phone } = this.props;
    return (
      <div className="card">
        <div className="row align-items-center justify-content-between">
          <div className="col-10">
            <Link to={`/users/${id}`}>{`${name} ${surname}`}</Link>
            <p>{ email }</p>
            <p>{ phone }</p>
          </div>
          <i id={id} className="fa fa-trash-alt" onClick={this.onDelete} />
        </div>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

Participant.propTypes = propTypes;
Participant.defaultProps = defaultProps;
