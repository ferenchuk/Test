import React from 'react';
import { render } from 'react-dom';
import './styles/index.css';
import registerServiceWorker from './registerServiceWorker';
import rootReducer from './rootReducer';
import { createStore } from 'redux';
import Root from './Root';

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

render(
  <Root store={store} />,
  document.getElementById('root')
);
registerServiceWorker();
