import React, { Component } from 'react';

import Header from './components/header/Header';
import Main from './Main';

export default class App extends Component {
  render() {
    return (
      <div className="container">
          <Header />
          <Main />
      </div>
    );
  }
}
