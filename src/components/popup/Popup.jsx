import React, { Component } from 'react';

export default class Popup extends Component {
  constructor(props) {
    super(props);

    this.show = this.show.bind(this);
    this.close = this.close.bind(this);

    this.state = {
      isOpen: false,
    };
  }

  show() {
    this.setState({ isOpen: true });
  }

  close() {
    this.setState({ isOpen: false });
  }

  render() {
    const { title } = this.props;
    const { isOpen } = this.state;
    return (
      <div className={`modal ${isOpen ? 'fade show' : '' }`}>
        <div className="modal-dialog">
          <div className='modal-content'>
            <div className="modal-header">
              <h1>{title}</h1>
              <i className="fas fa-lg fa-times" onClick={this.close} />
            </div>
            <div className="modal-body">
              {this.props.children}
            </div>
            <div className="modal-footer" />
          </div>
        </div>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

Popup.propTypes = propTypes;
Popup.defaultProps = defaultProps;
