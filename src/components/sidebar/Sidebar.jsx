import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="col-lg-3">
        <div className="card">
          <ul>
            <li><Link to="/users">Users</Link></li>
            <li><Link to="/groups">Groups</Link></li>
          </ul>
        </div>
      </div>
    );
  }
}

const propTypes = {};
const defaultProps = {};

Sidebar.propTypes = propTypes;
Sidebar.defaultProps = defaultProps;
