import React from 'react';

const Header = () => {
  return (
    <div className="row">
      <div className="col-lg-12">
        <h1 className="text-center card">IT Conference</h1>
      </div>
    </div>
  );
};

const propTypes = {};
const defaultProps = {};

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;