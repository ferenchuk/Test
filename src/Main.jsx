import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Sidebar from './components/sidebar/Sidebar';

import Participants from './stories/participants/containers/List';
import UserPreview from './stories/participants/containers/Preview';
import Groups from './stories/groups/containers/List';
import GroupPreview from './stories/groups/containers/Preview';

const Main = () => (
  <div className="row">
    <Sidebar />
    <div className="col-lg-9">
      <Switch>
        <Route exact path='/users' component={Participants} />
        <Route path='/users/:id' component={UserPreview} />
        <Route exact path='/groups' component={Groups} />
        <Route path='/groups/:id' component={GroupPreview} />
        <Redirect from='/' to='/users' />
      </Switch>
    </div>
  </div>
);

export default Main;
