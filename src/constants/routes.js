export const API_URL = 'http://localhost:3001';
export const API_GET_PARTICIPANTS_LIST = `${API_URL}/participants`;
export const API_GET_PARTICIPANT = `${API_URL}/participants`;
export const API_DELETE_PARTICIPANT = `${API_URL}/participants`;
export const API_CREATE_PARTICIPANT = `${API_URL}/participants`;
export const API_EDIT_PARTICIPANT = `${API_URL}/participants`;

export const API_FETCH_GROUPS = `${API_URL}/groups`;
export const API_CREATE_GROUP = `${API_URL}/groups`;
export const API_DELETE_GROUP = `${API_URL}/groups`;
export const API_UPDATE_GROUP = `${API_URL}/groups`;
