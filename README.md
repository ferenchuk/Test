## Start

Welcome! I hope that you like my work, so, let's go.

First of all, Thank you for this task.
Project was divided into 2 stories - Users & Groups.
I've used 2 different methods in my work, so, you can see Users section, which
was created using only React, and Groups tab using Redux state container.
## First of all

We must to clone repo by typing

```
git clone git@gitlab.com:ferenchuk/Test.git
```

## Installing dependencies

```
npm i
```

## Start server and project

Be sure, that you have installed json-server globally

```
npm i -g json-server
```

Then, go to project directory and start server and project
```
json-server --watch db.json
npm run start
```

## Production Build

To make production build, please, start next command: 
```
npm run build
```